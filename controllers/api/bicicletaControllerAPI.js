const Bicicleta = require("../../models/bicicleta");

exports.bicicleta_list = function (req, res) {
  Bicicleta.find({}, (err, bicis) => {
    res.status(200).json({
      bicicletas: bicis,
    });
  });
};

exports.bicicleta_insert = function (req, res) {
  var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
  bici.ubicacion = [req.body.lat, req.body.lng];
  Bicicleta.add(bici);

  res.status(200).json({
    bicicleta: bici,
  });
};

exports.bicicleta_update = function (req, res) {
  var bici = Bicicleta.findByCode(req.params.id);
  bici.color = req.body.color;
  bici.modelo = req.body.modelo;
  bici.ubicacion = [req.body.lat, req.body.lng];

  Bicicleta.update(bici, (err, raw) => {
    res.status(200).json({
      bicicleta: bici,
    });
  });

  res.status(200).json({
    bicicleta: bici,
  });
};

exports.bicicleta_delete = function (req, res) {
  Bicicleta.removeByCode(req.body.id, (err, cb) => {
    res.status(200).send();
  });
};
