var Bicicleta = require("../models/bicicleta");
exports.bicicleta_list = function(req,res){
    res.render("bicicletas/index",{ bicis : Bicicleta.allBicis});
}

exports.bicicleta_create_get = function(req, res){
    res.render("bicicletas/create");
}
exports.bicicleta_create_post = function(req, res){
    debugger;
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);
    res.redirect("/bicicletas");
}

exports.bicicleta_editar_get = function(req, res){
    console.log(req.params.id);
    var bici = Bicicleta.findById(req.params.id);
    res.render("bicicletas/editar",{bici});
}
exports.bicicleta_editar_post = function(req, res){
    var bici = Bicicleta.findById(req.params.id);
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    res.redirect("/bicicletas");
}

exports.bicicleta_delete_porst = function(req,resp){
Bicicleta.removeById(req.body.id);
resp.redirect("/bicicletas");

}