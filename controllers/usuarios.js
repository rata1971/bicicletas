var Usuario = require("../models/usuario");

module.exports = {
    list: function(req, res, next) {
        Usuario.find({}, (err, usuarios) => {
            res.render("usuarios/index",{ usuarios : usuarios});
        });
    },
    update_get: function(req, res, next) {
        Usuario.findById(req.params.id, function(err, usuario) {
            res.render("usuarios/update",{ errors: {}, usuario : usuario});
        });
    },
    update: function(req, res, next) {
        var update_values = {nombre: req.body.nombre};
        console.log(update_values);
        Usuario.findByIdAndUpdate(req.params.id, update_values, 
                function(err, usuario) {
                    if (err) {
                        console.log(err);
                        res.render('usuarios/update', {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
                    } else {
                        res.redirect('/usuarios');
                        return;
                    }
                });
    },
    create_get: function(req, res, next) {
        res.render('usuarios/create', {errors:{}, usuario: new Usuario()});
    },
    create: function(req, res, next) {
        if ( req.body.password != req.body.confirm_password) {
            res.render('usuarios/create', {errors: {confirm_password: {message: 'No coinciden las contraseñas'}}, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            return;
        }

        Usuario.create({nombre: req.body.nombre,
                        email: req.body.email,
                        password: req.body.password
                    }, function(err, nuevoUsuario) {
            if (err) {
                console.log("Error creando " , err.errors);
                res.render('usuarios/create', {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            } else {
                nuevoUsuario.enviar_email_bienvenida();
                res.redirect('/usuarios');
            }
        });
    },
    delete: function(req, res, next) {
        Usuario.findByIdAndDelete(req.body.id, function(err) {
            if ( err ) {
                next(err);
            } else {
                res.redirect('/usuarios');
            }
        })
    },

    
}

exports.bicicleta_create_get = function(req, res){
    res.render("bicicletas/create");
}
exports.bicicleta_create_post = function(req, res){
    debugger;
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);
    res.redirect("/bicicletas");
}

exports.bicicleta_editar_get = function(req, res){
    console.log(req.params.id);
    var bici = Bicicleta.findById(req.params.id);
    res.render("bicicletas/editar",{bici});
}
exports.bicicleta_editar_post = function(req, res){
    var bici = Bicicleta.findById(req.params.id);
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    res.redirect("/bicicletas");
}

exports.bicicleta_delete_post = function(req,resp){
Bicicleta.removeById(req.body.id);
resp.redirect("/bicicletas");

}