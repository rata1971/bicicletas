var mongoose = require("mongoose");
var schema = mongoose.Schema;


var sch_bicicleta = new schema({

    code : Number,
    color : String,
    modelo : String,
    ubicacion : {
        type : [Number], index : {type: '2dsphere', sparse : true}
    }

});

// static indica que agregamos el método directo al modelo
sch_bicicleta.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code : code,
        color : color,
        modelo : modelo,
        ubicacion : ubicacion
    });
}

// metodos del esquema
sch_bicicleta.methods.toString=function(){
return 'code ' +this.code+ ' | color ' + this.color;
}

// static indica que agregamos el método directo al modelo
sch_bicicleta.statics.allBicis = function(cb){
    return this.find({},cb);
}

sch_bicicleta.statics.add = function(bici, cb){
    this.create(bici,cb);
};

sch_bicicleta.statics.update = function(aBici, cb){ 
    this.replaceOne({code : aBici.code}, aBici, cb);
};

sch_bicicleta.statics.findByCode = function(code, cb){
    return this.findOne({code : code}, cb);
};
sch_bicicleta.statics.removeByCode = function(code, cb){
    return this.deleteOne({code : code}, cb);
};

module.exports = mongoose.model("Bicicleta", sch_bicicleta);


// var Bicicleta = function(id, color, modelo, ubicacion){
//     this.id = id;
//     this.color = color;
//     this.modelo = modelo;
//     this.ubicacion = ubicacion;
// }

// Bicicleta.prototype.toString = function(){
//     return 'id: '+this.id +" | color :"+this.color;
// }

// Bicicleta.allBicis = [];
// Bicicleta.add = function(aBici){
//     Bicicleta.allBicis.push(aBici);
// }
// Bicicleta.findById = function(aBiciId){
//     debugger;
//     var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
//     if(aBici) return aBici;
//     return null;
//     //throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
// };

// Bicicleta.removeById = function (aBiciId){

//     for(var i = 0; i < Bicicleta.allBicis.length ; i++){
//         if(Bicicleta.allBicis[i].id === aBiciId)
//         {
//             Bicicleta.allBicis.splice(i,1);
//             break;
//         }

//     }

// }

// var a  = new Bicicleta(1,'rojo','urbana',[11.859435, -86.218476]);
// var b  = new Bicicleta(2,'blanco','velocidad',[11.843271661553386,-86.18871443782848]);
// // Bicicleta.add(a);
// // Bicicleta.add(b);
// module.exports = Bicicleta;