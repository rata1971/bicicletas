var mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
var Reserva = require("./reserva");
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const saltRounds = 10;

const Token = require('../models/token');
const mailer = require('../mailer/mailer');
const { callbackPromise } = require("nodemailer/lib/shared");

var Schema = mongoose.Schema;

const validateEmail = function(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

var sch_usuario = new Schema({
    nombre : {
        type: String, 
        trim: true,
        reqired: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        reqired: [ true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un email valido'],
        match: [/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/]
    },
    password: {
        type: String,
        required: [true, 'La contraseña es obligatoria']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

sch_usuario.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario'});


sch_usuario.pre('save', function(next) {
    if ( this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

sch_usuario.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}

sch_usuario.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({
        usuario : this._id, bicicleta : biciId, desde:desde, hasta: hasta
    });
    console.log(reserva);
    reserva.save(cb);
}


sch_usuario.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({_userId: this.id, 
                            token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function (err) {
        if ( err ) { 
            return console.log(err.message);
        }
        const emailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificacion de cuenta',
            text: 'Hola,\n\n' + 
            'Por favor, para verificar su cuenta haga click en este link \n' + 
                        'http://localhost:3000' + 
                        '\/token/confirmation\/' + 
                        token.token + '\n'
        };
        mailer.sendMail(emailOptions, function(err) {
            if ( err) {
                return console.log(err.message);
            }
            console.log('Un email de verificacion ha sido enviado a ' + email_destination);
        })
        cb(null);
    })
                        
}

sch_usuario.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);
    self.findOne({
        $or: [
            {'googleId': condition.id},
            {'email': condition.emails[0].value}
        ]}, (err, result) => {
            if ( result ) {
                callback(err, result)
            } else {
                console.log('------------CONDITION ------------');
                console.log(condition);
                let values = {};
                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = condition._json.etag;
                console.log('----------- VALUES ------------');
                console.log(values);
                self.create(values, (err, result) => {
                    if (err) { console.log(err); }
                    return callbackPromise(err, result)
                })
            }
        }

    )
}

sch_usuario.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);
    self.findOne({
        $or: [
            {'facebookId': condition.id},
            {'email': condition.emails[0].value}
        ]}, (err, result) => {
            if ( result ) {
                callback(err, result)
            } else {
                console.log('------------CONDITION ------------');
                console.log(condition);
                let values = {};
                values.facebookId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex');
                console.log('----------- VALUES ------------');
                console.log(values);
                self.create(values, (err, result) => {
                    if (err) { console.log(err); }
                    return callback(err, result)
                })
            }
        }

    )
}


module.exports = mongoose.model("Usuario", sch_usuario);


/*

facebok desde postman
access_token: EAADwIAlDWTQBAIi6LCKZCzaEZBupveq02VvrnROFEZANRqZCoLIvdzBKwMShKDPLYohZBQLglWQmPGio1UnnpQfFtwfkb0ps1S4XHLZB5t6zTZBuYu3dNydj4EtNJIklZAmeDxpRoViBdy1mGWwOkrDcW2B238Bw06xE2T2dEZAbhx3gfuBZAiSSsiSfJCBwA1BSttcEREiBMHYAZDZD


*/