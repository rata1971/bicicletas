

var mymap = L.map('mapid').setView([-34.615513, -58.542020], 13);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
    attibution :'&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> Contributors'
}).addTo(mymap);


$.ajax({
    dataType:'json',
    url : 'api/bicicletas',
    success:function(data){
        
        data.bicicletas.forEach(element => {
            L.marker(element.ubicacion,{title:element.id}).addTo(mymap);
        });
    }

});