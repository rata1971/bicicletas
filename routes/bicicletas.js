var express = require("express");
var router = express.Router();
var bicicletas = require("../controllers/bicicleta");
router.get("/", bicicletas.bicicleta_list);
router.get("/create", bicicletas.bicicleta_create_get);
router.post("/create", bicicletas.bicicleta_create_post);
router.post("/:id/delete", bicicletas.bicicleta_delete_porst);

router.get("/:id/editar", bicicletas.bicicleta_editar_get);
router.post("/:id/editar", bicicletas.bicicleta_editar_post);

module.exports = router;